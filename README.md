# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Quick summary
SERVICE - Sistema de unidades de farmácias 
Version
1.0


### How do I get set up? ###

Summary of set up
To start webservice up correctly is necessary to upload this project with maven project.  

Configuration
It's developed with Spring boot, Spring Data and MySql databases.

Database configuration
To use this service in mysql is necessary to create a name schema: apirest 

### How to run tests ###
After does instructions below, starts Application.class with Java Application
Path: Right click in the archive, Run As -> Java Appication

if you would like to have a ready script, you can use this below

insert into farmacia(id,cnpj,endereco,nome) values (1,'40.062.605/0001-21','Rua Tamandaré, 1503, Centro, Juazeiro do Norte - CE','FarmaJua');
insert into farmacia(id,cnpj,endereco,nome) values (2,'40.062.605/0002-21','Rua São José, 486, Centro, Ribeirão Preto - SP','FarmaRib');
insert into farmacia(id,cnpj,endereco,nome) values (3,'40.062.605/0003-21','Av. Alberto Andaló, 2586, Centro, São José do Rio Preto - SP','FarmaSJRP');
insert into farmacia(id,cnpj,endereco,nome) values (4,'40.062.605/0004-21','Av. Navarro de Andrade, 1500, Centro, Salvador - BA','FarmaOxi');
insert into farmacia(id,cnpj,endereco,nome) values (5,'40.062.605/0005-21','Rua São João, 330, Centro, Porto Alegre - RS','FarmaTche');
insert into farmacia(id,cnpj,endereco,nome) values (6,'40.062.605/0006-21','Av. Afonso Pena, 3599, Centro, Belo Horizonte - MG','FarmaUai');

insert into laboratorio(id,cnpj,endereco,nome,telefone) values (1,'47.896.129/0001-93','Rua Eliseu Guilherme, 1548, Boulevard, Ribeirão Preto - SP','Eurofarma','16-3625-5846');
insert into laboratorio(id,cnpj,endereco,nome,telefone) values (2,'46.554.310/0001-59','Rua Barão do Amazonas, 482, Centro, Ribeirão Preto - SP','Medley','16-3625-8966');


insert into medicamento(id, dosagem, generico, lote, nome, quantidade, validade, farmacia_id, laboratorio_id) values (1,'100ml',true,'184864-5','Tylenol',100,now(),1,1);
insert into medicamento(id, dosagem, generico, lote, nome, quantidade, validade, farmacia_id, laboratorio_id) values (2,'200g',false,'38354-5','Nimesulida',100,now(),2,2);
insert into medicamento(id, dosagem, generico, lote, nome, quantidade, validade, farmacia_id, laboratorio_id) values (3,'100ml',true,'184864-5','Tylenol',100,now(),4,1);
insert into medicamento(id, dosagem, generico, lote, nome, quantidade, validade, farmacia_id, laboratorio_id) values (4,'100ml',true,'184224-5','Tylenol',30,now(),6,1);


Well done, the service is working, feel free to test this...



