package br.com.apirest.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.apirest.model.Medicamento;

/* Essa repository tem a função de chamar funções do Spring boot como PUT, GET, DELETE, POST estendendo a 
 * interface PagingAndSortingRepository que já contem os metodos do CRUD prontos, sendo apenas necessário fazer a requisição 
 * HTTP com o tipo de método que quer chamar.
 * 
 *  Disponibilizei também uma consulta customizada para pegar uma lista de objetos pelo nome do mesmo.
 *  */

@RepositoryRestResource(collectionResourceRel = "medicamento", path = "medicamentos")
public interface MedicamentoRepository extends PagingAndSortingRepository<Medicamento, Long>  {
	
	List<Medicamento> findByNome(@Param("name") String name);
	
	//Requisições GET; POST; PUT E DELETE já estendidas pela classe PagingAndSortingRepository, não sendo necessário declará-las... 
	
}
