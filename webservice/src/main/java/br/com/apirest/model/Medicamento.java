package br.com.apirest.model;

import java.util.Date;

/* Modelo da entidade Medicamento */

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Medicamento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)    
	private Long id;
	
	private String nome;
	private boolean generico;
	private String dosagem;
	private Date validade;
	private String lote;
	private Integer quantidade;
	
	@OneToOne
	@JoinColumn(name="laboratorio_id")
	private Laboratorio laboratorio;
	
	@OneToOne
	@JoinColumn(name="farmacia_id")
	private Farmacia farmacia;
	
	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public boolean isGenerico() {
		return generico;
	}
	
	public void setGenerico(boolean generico) {
		this.generico = generico;
	}
	
	public String getDosagem() {
		return dosagem;
	}

	public void setDosagem(String dosagem) {
		this.dosagem = dosagem;
	}

	public Date getValidade() {
		return validade;
	}
	
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	
	public String getLote() {
		return lote;
	}
	
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	public Integer getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Laboratorio getLaboratorio() {
		return laboratorio;
	}
	
	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public Farmacia getFarmacia() {
		return farmacia;
	}

	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}
	
}
