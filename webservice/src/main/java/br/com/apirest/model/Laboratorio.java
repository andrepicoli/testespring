package br.com.apirest.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/* Modelo da entidade Laboratorio */

@Entity
public class Laboratorio {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	private Long id;
	
	private String nome;
	private String cnpj;
	private String endereco;
	private String telefone;
	/*
	@OneToMany(targetEntity=Medicamento.class, fetch=FetchType.EAGER)
	@JoinColumn(name="medicamento_id")
	private List<Medicamento> medicamentos;
*/
	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

/*
	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}
	*/
}
