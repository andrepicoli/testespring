package br.com.apirest.run;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/* 
 * Essa classe é responsável por startar a service.
 * Necessário dar botão direito na mesma, Run as, Java Application para startar
 * 
 */

@SpringBootApplication
@EntityScan(basePackages = {"br.com.apirest.model"})
@EnableJpaRepositories(basePackages = {"br.com.apirest.repository"})
@EnableScheduling
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);    
	}

}